## Test Project

- `composer install` to install vendor files
- `cp .env.example .env` create environment file
- Change environment values from .env file
- `php artisan config:cache`  to cache updated variables
- `php artisan migrate --seed` create db tables and insert dummy data
- `php artisan serve` to serve the project (default port: 8000)

========================================================
- Visit `localhost:8000` to register or login user
- `/setfoo` to store foo data
- `/emails` to view list of 50 email
- click the button to store emails to cache
- `php artisan test:emails` to run schedules to send mails with queue
- `php artisan queue:work` to start the queue work

========================================================

- optional: `/logs`  to view log files

