<?php

namespace App\Console\Commands;

use App\EmailList;
use App\Jobs\SendEmailJob;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class TestEmailSend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a email to test email address';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $emails = Cache::get('emails');

        if (!$emails) {
            return false;
        }

        foreach ($emails as $email){
            foreach ($email as $item) {
                dispatch(new SendEmailJob($item));
            }
        }
    }
}
