<?php

namespace App\Http\Controllers;

use App\EmailList;
use App\Jobs\SendEmailJob;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MailManagerController extends Controller
{
    public function emailLists()
    {
        $emails = EmailList::all()->take(50);

        return view('mail.list', [
            'emails' => $emails
        ]);
    }


    public function sendEmails()
    {
        Cache::forget('emails');
        Cache::rememberForever('emails', function () {
            return EmailList::all()->take(50)->chunk(10);
        });
    }
}
