<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SetFooController extends Controller
{
    public function index()
    {
        return view('foo.set_foo');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'foo' => 'required',
        ], [
            'foo.required' => 'You must set foo to continue'
        ]);

        session()->put('foo', $request->foo);

        return redirect()->route('home');

    }
}
