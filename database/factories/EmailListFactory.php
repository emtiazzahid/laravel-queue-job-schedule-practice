<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\EmailList::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
    ];
});