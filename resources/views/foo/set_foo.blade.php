@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Set Foo</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                            <form method="post" action="{{ route('fooStore') }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Foo Value</label>
                                    <input type="text" class="form-control" name="foo" id="foo" aria-describedby="foo" placeholder="Enter foo value">
                                    @if ($errors->has('foo'))
                                        <div class="invalid-feedback" style="display: block">
                                            {{ $errors->first('foo') }}
                                        </div>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
