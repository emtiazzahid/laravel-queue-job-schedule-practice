@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Mail List</div>

                    <div class="card-body">
                        <ul>
                        @foreach($emails as $item)
                            <li>{{ $item->email }}</li>
                        @endforeach
                        </ul>


                        <a href="{{ route('sendEmails') }}" class="btn btn-primary">Submit</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
