<?php


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['web', 'auth']], function () {

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    Route::get('/setfoo', ['uses' => 'SetFooController@index', 'as' => 'setFoo']);

    Route::post('/setfoo/store', ['uses' => 'SetFooController@store', 'as' => 'fooStore']);

    Route::group(['middleware' => ['foo_check']], function () {
        Route::get('/home', 'HomeController@index')->name('home');

        Route::get('/emails', 'MailManagerController@emailLists')->name('emailLists');

        Route::get('/emails/send', 'MailManagerController@sendEmails')->name('sendEmails');


    });
});