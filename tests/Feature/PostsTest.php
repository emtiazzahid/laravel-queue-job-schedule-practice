<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostsTest extends TestCase
{
    use DatabaseTransactions;

    public function testPostsCreation()
    {
        $title = 'Some subject!';
        $description = 'Some description';
        factory('App\EmailList')->create([
            'title' => $title,
            'description' => $description,
        ]);

        $this->assertDatabaseHas('posts', [
            'title' => $title,
            'description' => $description
        ]);
    }
}
